/*
 * UART.c
 *
 *  Created on: 22.11.2019
 *      Author: Dawid
 */
#include "UART.h"
#include <stdint.h>
#include "stm32f4xx_hal.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>









void dg_uart_send_data( char* debug_state, char* md_type, char* message,UART_HandleTypeDef *uart_config)
{
	char* whole_message;
	uint16_t length=0;
	uint16_t length_1=0;
	uint16_t length_2=0;
	uint16_t size=0;


	char sign='d';

	while(sign!='\0')
	{												//sprawdza d�ugosc pirwszego argumentu
		length_1++;
		sign=md_type[length_1];
	}

	sign='d';

	while(sign!='\0')								//sprawdza dlugosc trzeciego argumentu
	{
		length_2++;
		sign=message[length_2];
	}

	length=length_1+length_2+6+6;			//+6 bouwzgledniamy spacje oraz znaki konca linii i karetki i cudzyslowia
											//+6 sta�a slugosc stanu:ok,error,info


	whole_message= malloc(length*sizeof(char));		//dynamiczna alokacja pamieci o dlugosci calej wiadomosci


	for (uint16_t i;i<length;i++)					//zerujemy utworzony bufor
	{
		whole_message[i]=0;
	}









	if(!(strcmp(debug_state,"_OK_")))		//sprawdzamy czy stringi sa takie same kolorki
	{
		size=sprintf(whole_message,"\033[0;32m%-6s\033[0m %s \"%s\"\n\r",debug_state,md_type, message);

	}
	else if(!(strcmp(debug_state,"_ERR_")))
	{
		size=sprintf(whole_message,"\033[0;31m%-6s\033[0m %s \"%s\"\n\r",debug_state,md_type, message);
	}
	else
	{
		size=sprintf(whole_message,"\033[0;34m%-6s\033[0m %s \"%s\"\n\r",debug_state,md_type, message);
	}




	HAL_UART_Transmit_IT(uart_config,whole_message,size); //nadajemy przy u�yciu przerwa�

	free(whole_message);			//zwalniamy zaalokowana pamiec


}





